import React from 'react';
import { render } from "@testing-library/react";
import Button from "../../components/Button";

describe('Button', () => {
  it('should be able to render a button', () => {
    const { getByText } = render(<Button type="submit">Entrar</Button>)

    expect(getByText('Entrar')).toBeTruthy();
  });

  it('shows the expected text when loading', () => {
    const { getByText } = render(
    <Button loading={true} type="submit">
      Entrar
    </Button>
    )

    expect(getByText('Carregando...')).toBeTruthy();
  });
});
