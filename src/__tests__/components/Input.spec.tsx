import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';

import Input from '../../components/Input';
import { FiAlertCircle, FiLock } from 'react-icons/fi';
import { Error } from '../../components/Input/styles';

jest.mock('@unform/core', () => ({
  useField() {
    return {
      fieldName: 'email',
      defaultValue: '',
      error: '',
      registerField: jest.fn(),
    };
  },
}));

describe('Input component', () => {
  it('should be able to render an input', () => {
    const { getByPlaceholderText } = render(
      <Input name="email" placeholder="E-mail" />
    );

    expect(getByPlaceholderText('E-mail')).toBeTruthy();
  });

  it('should be able to render an input with a icon', () => {
    const { getByTestId } = render(
      <Input icon={FiLock} name="email" placeholder="E-mail" />
    );

    const iconInput = getByTestId('input-icon');

    expect(iconInput).toBeInTheDocument();
  });

  it('should be show an error when input is not filled', () => {
    const { getByPlaceholderText } = render(
      <Input name="email" placeholder="E-mail" />,
    );

    const errorComponent = render(
      <Error title="E-mail obrigatório">
          <FiAlertCircle size={20} color="#c53030" />
      </Error>
    );

    const inputElement = getByPlaceholderText('E-mail');

    fireEvent.change(inputElement, {
      target: {
        value: '',
      }
    });

    expect(errorComponent).toBeTruthy();
  });

  it('should render highlight on input focus', async () => {
    const { getByPlaceholderText, getByTestId } = render(
      <Input name="email" placeholder="E-mail" />
    );

    const inputElement = getByPlaceholderText('E-mail');
    const containerElement = getByTestId('input-container');

    fireEvent.focus(inputElement);

    await waitFor(() => {
      expect(containerElement).toHaveStyle('border-color: #0070F3');
      expect(containerElement).toHaveStyle('color: #0070F3');
    });

    fireEvent.blur(inputElement);

    await waitFor(() => {
      expect(containerElement).not.toHaveStyle('border-color: #0070F3');
      expect(containerElement).not.toHaveStyle('color: #0070F3');
    });
  });

  it('should keep input border highlight when input filled', async () => {
    const { getByPlaceholderText, getByTestId } = render(
      <Input name="email" placeholder="E-mail" />,
    );

    const inputElement = getByPlaceholderText('E-mail');
    const containerElement = getByTestId('input-container');

    fireEvent.change(inputElement, {
      target: {
        value: 'johndoe@example.com',
      },
    });

    fireEvent.blur(inputElement);

    await waitFor(() => {
      expect(containerElement).toHaveStyle('color: #0070F3');
    });
  });
});
