import { renderHook, act } from '@testing-library/react-hooks';

import { ToastProvider, useToast } from '../../hooks/toast'

describe('Toast Hook', () => {
  it('should add toast', async () => {
    const { result, waitForNextUpdate,  } = renderHook(() => useToast(), {
      wrapper: ToastProvider
    });

    act(() => {
      result.current.addToast({
        type: 'success',
        title: 'test-success',
        description: 'test-description'
      })
    });

    await waitForNextUpdate();
  });
});
