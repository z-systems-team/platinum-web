import styled, { css } from 'styled-components';

import Tooltip from '../Tooltip';

interface ContainerProps {
  isFocused: boolean;
  isFilled: boolean;
  isErrored: boolean;
}

export const Container = styled.div<ContainerProps>`
  border-radius: 8px;
  border: 1px solid #C3C3C3;
  padding: 16px;
  width: 100%;
  color: #C3C3C3;
  display: flex;
  align-items: center;

  & + div {
    margin-top: 8px;
  }

  ${(props) =>
    props.isErrored &&
    css`
      border-color: #c53030;
    `}

  ${(props) =>
    props.isFocused &&
    css`
      color: #0070F3;
      border-color: #0070F3;
    `}

  ${(props) =>
    props.isFilled &&
    css`
      color: #0070F3;
    `}

  input {
    flex: 1;
    border: 0;
    background: transparent;
    color: #666360;

    &::placeholder {
      color: #c3c3c3;
    }
  }

  svg {
    margin-right: 16px;
  }
`;

export const Error = styled(Tooltip)`
  height: 20px;
  margin-left: 16px;

  svg {
    margin: 0;
  }

  span {
    background: #c53030;
    color: #fff;
    &::before {
      border-color: #c53030 transparent;
    }
  }
`;
