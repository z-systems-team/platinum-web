import styled from 'styled-components';
import { shade } from 'polished';

export const Container = styled.button`
  height: 56px;
  background: var(--secondary-color);
  box-shadow: rgba(0, 94, 204, 0.39) 0px 4px 14px 0px;
  border-radius: 8px;
  border: 0;
  padding: 0 16px;
  color: var(--primary-color);
  width: 100%;
  font-weight: 500;
  margin-top: 16px;
  transition: background 0.2s;

  &:hover {
    background: ${shade(0.1, '#0070F3')};
  }
`;
