import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  :root {
    --primary-color: #FFF;
    --secondary-color: #0070F3;
    --font-family: 'Roboto Slab', sans-serif;
  }

  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  ul,
  li {
  list-style: none;
  }


  a {
    text-decoration: none;
    color: var(--secondary-color);
  }

  body {
    color: #333;
    -webkit-font-smoothing: antialiased;
  }

  body, input, button {
    font-family: var(--font-family);
    font-size: 16px;
  }
  h1, h2, h3, h4, h5, h6, strong {
    font-weight: 500;
  }

  button {
    cursor: pointer;
  }
`;
